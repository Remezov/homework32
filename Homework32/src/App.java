public class App {
    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.insert(30, "q");
        tree.insert(20, "w");
        tree.insert(40, "e");
        tree.insert(11, "r");
        tree.insert(15, "t");
        tree.insert(54, "y");
        tree.insert(76, "a");
        tree.insert(25, "s");
        tree.insert(53, "d");
        tree.insert(1, "f");
        System.out.printf("There are %d leave(s).", tree.findLeave(tree.getRoot()));
    }

}
