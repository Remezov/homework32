public class Tree {
    private Node root;
    private static int countLeave = 0;

    public Node getRoot() {
        return root;
    }

    public void insert(int key, String data){
        Node node = new Node();
        node.setKey(key);
        node.setData(data);
        if(root==null){
            root = node;
        }else{
            Node current = root;
            Node prev = null;
            while (true){
                prev = current;
                if(key<prev.getKey()){
                    current = current.getLeftChild();
                    if(current==null){
                        prev.setLeftChild(node);
                        return;
                    }
                }else{
                    current = current.getRightChild();
                    if(current==null){
                        prev.setRightChild(node);
                        return;
                    }
                }
            }
        }
    }

    public int findLeave(Node root){
        if(root != null){
            findLeave(root.getLeftChild());
            if ((root.getLeftChild() == null) && (root.getRightChild() == null)) {
                countLeave++;
            }
            findLeave(root.getRightChild());
        }
        return countLeave;
    }
}
